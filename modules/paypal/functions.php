<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_SYSTEM')) {
    exit('Stop!!!');
}

define('NV_IS_MOD_PAYPAL', true);

global $global_array_cat;
$global_array_cat = [];
$catid = 0;
$parentid = 0;
$alias_cat_url = isset($array_op[0]) ? $array_op[0] : '';
$array_mod_title = [];

$global_code_defined = [
    'cat_visible_status' => [1, 2],
    'cat_locked_status' => 10,
    'row_locked_status' => 20,
    'edit_timeout' => 180
];

$sql = "SELECT * FROM nv4_vi_news_cat WHERE status IN(" . implode(',', $global_code_defined['cat_visible_status']) . ") ORDER BY sort ASC";
$list = $nv_Cache->db($sql, 'catid', $module_name);
// print_r($list);die;
if (!empty($list)) {
    foreach ($list as $l) {
        $global_array_cat[$l['catid']] = $l;
        $global_array_cat[$l['catid']]['link'] = NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module_name . '&amp;' . NV_OP_VARIABLE . '=' . $l['alias'];
        if ($alias_cat_url == $l['alias']) {
            $catid = $l['catid'];
            $parentid = $l['parentid'];
        }
    }
}

unset($result, $catid_i, $parentid_i, $title_i, $alias_i);

$module_info['submenu'] = 0;

$page = 1;

$count_op = sizeof($array_op);
if (!empty($array_op) and $op == 'San-pham' or $op == 'main') {
    $parentid = $catid;
    while ($parentid > 0) {
        $array_cat_i = $global_array_cat[$parentid];
        $array_mod_title[] = [
            'catid' => $parentid,
            'title' => $array_cat_i['title'],
            'link' => $array_cat_i['link']
        ];
        $parentid = $array_cat_i['parentid'];
    }
    krsort($array_mod_title, SORT_NUMERIC);
}